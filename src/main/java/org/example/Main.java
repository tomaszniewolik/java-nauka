package org.example;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) throws IOException {
        System.out.println("Hello world!");
        int a = 23, b = 33;
        double d = 1.2;
        Integer aa = 23;
        Integer bb = 33;

        System.out.println(a + b);
        System.out.println(aa + bb);

        for (int i=0; i<5; i++) {
            System.out.println("Java jak c++ id");
        }

        for(int i=0; i< args.length; i++) {
            System.out.println(args[i]);
        }

        for(String s: args) {
            System.out.println(s);
        }

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String s1 = reader.readLine();
        Scanner scanner = new Scanner(System.in);
        scanner.nextLine();
        String s2 = scanner.nextLine();

        System.out.println("reader.readLine() = " + s1);
        System.out.println("scanner.nextLine() = " + s2);

    }
}