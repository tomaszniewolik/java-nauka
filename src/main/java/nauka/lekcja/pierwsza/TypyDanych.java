package nauka.lekcja.pierwsza;

import java.math.BigDecimal;
import java.math.BigInteger;

public class TypyDanych {

    public static void main(String[] args) {
        System.out.println("Lekcja pierwsza");
        int a;
        long b;
        char c;
        boolean d;
        double e;
        float f;
        byte g;

        Integer aa = 1;
        Long bb;
        Character cc;
        Boolean dd;
        Double ee;
        Float ff;
        Byte gg;

        BigInteger bi;
        BigDecimal bd;
        int i;
        for (i =1; i<5; i++) {
            System.out.println("FOR: " + i);
        }
        while (i>0) {
            System.out.println("WHILE: " + i);
            i--;
        }
        do {
            System.out.println("DO WHILE: " + i);
            i++;
        } while (i<5);

        while(true) {
            System.out.println("infinite " + i);
            i++;
            if (i>10) {
                break;
            }
        }
    }
}
