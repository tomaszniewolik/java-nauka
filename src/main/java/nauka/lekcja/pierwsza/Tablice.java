package nauka.lekcja.pierwsza;

import java.util.Arrays;

public class Tablice {
    public static void main(String[] args) {
        System.out.println("Tablice");

        int[] a = {1,2,3};

        System.out.println( a[1] );

        Integer[] aa = {1,2,3};
        System.out.println( aa[1] );

        for (int i=0;i<a.length;i++) {
            System.out.println( a[i] );
        }
        for (int i=0;i<a.length;i++) {
            System.out.println( aa[i] );
        }

        for (int x: a) {
            System.out.println( x );
        }
        for (Integer x: aa) {
            System.out.println( x );
        }

    }
}
