package nauka.lekcja.pierwsza;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class ZbioryIMapy {

    public static void main(String[] args) {
        System.out.println("Zbiory");
        Set<String> zbior = new HashSet<>();
        System.out.println(zbior.size());
        System.out.println(zbior.isEmpty());
        zbior.add("Pierwszy");
        zbior.add("Drugi");
        zbior.add("Trzeci");
        System.out.println(zbior.size());
        System.out.println(zbior.isEmpty());
        zbior.add("Pierwszy");
        System.out.println(zbior.size());
        System.out.println(zbior.isEmpty());

        for (String s: zbior) {
            System.out.println(s);
        }
        System.out.println("Mapy");

        Map<Integer, String> mapa = new HashMap<>();
        System.out.println(mapa.size());
        System.out.println(mapa.isEmpty());
        mapa.put(1, "Jeden");
        mapa.put(2, "Dwa");
        mapa.put(3, "Trzy");
        System.out.println(mapa.size());
        System.out.println(mapa.isEmpty());
        if (!mapa.containsKey(1)) {
            mapa.put(1, "Jeden Jeden");
            System.out.println(mapa.size());
            System.out.println(mapa.isEmpty());
        }
        mapa.put(11, "Jeden");
        System.out.println(mapa.size());
        System.out.println(mapa.isEmpty());

        for (Integer key: mapa.keySet()) {
            System.out.println(key );
//            System.out.println(key + " => " + mapa.get(key));
        }
        for (String val: mapa.values()) {
            System.out.println(val);
        }



    }
}
