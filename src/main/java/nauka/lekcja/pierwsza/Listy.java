package nauka.lekcja.pierwsza;

import java.util.ArrayList;
import java.util.List;

public class Listy {

    public static void main(String[] args) {
        System.out.println("Listy");
        List<String> listaStringow = new ArrayList<>();
        System.out.println(listaStringow.size());
        listaStringow.add("Jeden");
        listaStringow.add("Dwa");
        listaStringow.add("Trzy");
        System.out.println(listaStringow.size());
        listaStringow.add("Jeden");
        System.out.println(listaStringow.size());
        for(int i=0;i<listaStringow.size();i++) {
            System.out.println(listaStringow.get(i));
        }
        for(String s: listaStringow) {
            System.out.println(s);
        }
    }
}
